# Reforestation in drylands

This project will contain the scripts to identify large-scale (100 km2 or more) reforested areas in African drylands based upon MODIS vcf data (MOD44B) and other relevant remote sensing products.   